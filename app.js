const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

// Promise based package

async function getAllCountries() {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        // Get all countries

        const countries = await sequelize.query('SELECT * FROM country', {
            type: QueryTypes.SELECT
        });

        const countryNames = countries.map(country => country.Name);
        console.log(countryNames);
        await sequelize.close();
        
    } catch (e) {
        console.error(e);   
    }
    
}

async function getAllCities() {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        // Get all cities

        const cities = await sequelize.query('SELECT * FROM city', {
            type: QueryTypes.SELECT
        });

        const cityNames = cities.map(city => city.Name);
        console.log(cityNames);
        await sequelize.close();
        
    } catch (e) {
        console.error(e);   
    }
    
}

async function getAllLanguages() {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        // Get all languages

        const languages = await sequelize.query('SELECT * FROM countrylanguage', {
            type: QueryTypes.SELECT
        });

        const languageNames = languages.map(language => language.Language);
        console.log(languageNames);
        await sequelize.close();
        
    } catch (e) {
        console.error(e);   
    }
    
}

getAllCountries();
getAllCities();
getAllLanguages();


